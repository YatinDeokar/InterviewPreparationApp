package com.example.deokar.interviewpreparationapp.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deokar.interviewpreparationapp.utils.Config;
import com.example.deokar.interviewpreparationapp.R;
import com.example.deokar.interviewpreparationapp.activity.MainActivity;


public class OptionListFragment extends Fragment implements View.OnClickListener {

    String TAG = OptionListFragment.class.getSimpleName();

    View view;

    Toolbar toolbar;

    LinearLayout bannerAddLayout;

    LinearLayout optionOneLayout, optionTwoLayout, optionThreeLayout, optionFourLayout,
            optionFiveLayout, optionSixLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_option_list, container, false);

        initView();

        return view;
    }

    /**
     * Initialise views and listeners
     */
    private void initView() {

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);


        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);


        optionOneLayout = view.findViewById(R.id.option_layout_one);
        optionTwoLayout = view.findViewById(R.id.option_layout_two);
        optionThreeLayout = view.findViewById(R.id.option_layout_three);
        optionFourLayout = view.findViewById(R.id.option_layout_four);
        optionFiveLayout = view.findViewById(R.id.option_layout_five);
        optionSixLayout = view.findViewById(R.id.option_layout_six);

        bannerAddLayout = view.findViewById(R.id.banner_ad_layout);
        TextView textView = view.findViewById(R.id.ad_text_view);
        textView.setSelected(true);

        optionOneLayout.setOnClickListener(this);
        optionTwoLayout.setOnClickListener(this);
        optionThreeLayout.setOnClickListener(this);
        optionFourLayout.setOnClickListener(this);
        optionFiveLayout.setOnClickListener(this);
        optionSixLayout.setOnClickListener(this);
        bannerAddLayout.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {

            case R.id.option_layout_one:


                MainActivity.questionListFragment();

                break;

            case R.id.option_layout_two:

                Toast.makeText(getContext(), Config.NO_AVAILABLE_MESSAGE, Toast.LENGTH_SHORT).show();

                break;

            case R.id.option_layout_three:

                Toast.makeText(getContext(), Config.NO_AVAILABLE_MESSAGE, Toast.LENGTH_SHORT).show();

                break;

            case R.id.option_layout_four:

                Toast.makeText(getContext(), Config.NO_AVAILABLE_MESSAGE, Toast.LENGTH_SHORT).show();

                break;

            case R.id.option_layout_five:

                Toast.makeText(getContext(), Config.NO_AVAILABLE_MESSAGE, Toast.LENGTH_SHORT).show();

                break;

            case R.id.option_layout_six:

                Toast.makeText(getContext(), Config.NO_AVAILABLE_MESSAGE, Toast.LENGTH_SHORT).show();

                break;

            case R.id.banner_ad_layout:

                if (Config.checkConnectivityShowMessage(getContext()))
                    Config.openWebsiteInBrowser(getContext(), Config.AD_URL);

                break;


        }


    }

    /**
     * Blink animation effect
     * @param view
     */
    private void manageBlinkEffect(View view) {
        view.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.blink));
    }


    /**
     * Close page conformation dialog
     */
    private void closeConformationDialog() {

        AlertDialog s = new AlertDialog.Builder(getContext(), android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth)
                .setTitle("Close")
                .setMessage(Config.CLOSE_APP_CONFORMATION_MESSAGE)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // close activity
                        getActivity().finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();


    }


}
