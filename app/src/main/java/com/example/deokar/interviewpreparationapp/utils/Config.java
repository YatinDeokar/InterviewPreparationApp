package com.example.deokar.interviewpreparationapp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.widget.Toast;

import com.example.deokar.interviewpreparationapp.ConnectivityReceiver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Config {

    public static final String AD_URL = "https://courses.learncodeonline.in";
    public static final String QUESTION_DATA_API = "https://learncodeonline.in/api/android/datastructure.json";

    //List Text
    public static final float ITEM_TITLE_TEXT_SIZE = 21f;
    public static final float ITEM_ROW_PRIMARY_TEXT_SIZE = 16f;
    public static final float ITEM_ROW_SECONDARY_TEXT_SIZE = 14f;

    public static final String NO_INTERNET_MESSAGE = "Please connect to internet.";
    public static final String NO_AVAILABLE_MESSAGE = "Currently not available.";
    public static final String CLOSE_APP_CONFORMATION_MESSAGE = "Are you sure you want to close the app.";

    public static String formatToLocalDay(Date dateTime) {
        SimpleDateFormat localDayFormat = new SimpleDateFormat("E",
                Locale.getDefault());
        return localDayFormat.format(dateTime);
    }

    /**
     * Get roboto light font
     *
     * @param context
     * @return
     */
    public static Typeface typeFaceRobotoLight(Context context) {
        return setTypeface(context.getAssets(), "Roboto-Light.ttf");
    }

    /**
     * Get roboto regular font
     *
     * @param context
     * @return
     */
    public static Typeface typeFaceRobotoRegular(Context context) {
        return setTypeface(context.getAssets(), "Roboto-Regular.ttf");
    }


    private static Typeface setTypeface(AssetManager asset, String typeface) {
        return Typeface.createFromAsset(asset, typeface);
    }

    /**
     * Open website link browser
     */
    public static void openWebsiteInBrowser(Context context, String url) {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);

        //pass the url to intent data
        intent.setData(Uri.parse(url));

        context.startActivity(intent);
    }

    /**
     * Check Internet connection
     * @param context
     * @return
     */
    public static boolean checkConnectivityShowMessage(Context context) {
        boolean isConnected;
        if (ConnectivityReceiver.isConnected()) {
            isConnected = true;
        } else {
            Toast.makeText(context, NO_INTERNET_MESSAGE, Toast.LENGTH_SHORT).show();
            isConnected = false;
        }
        return isConnected;
    }

}
