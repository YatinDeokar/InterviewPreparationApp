package com.example.deokar.interviewpreparationapp.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deokar.interviewpreparationapp.R;
import com.example.deokar.interviewpreparationapp.utils.Config;

public class ThemeAndStyle {

    /**
     * Style for primary text
     *
     * @param mContext
     * @param textView
     */
    public static void setListPrimaryTextStyle(Context mContext, TextView textView) {

        textView.setTypeface(Config.typeFaceRobotoRegular(mContext));
        textView.setTextSize(Config.ITEM_ROW_PRIMARY_TEXT_SIZE);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.darkBlack));

    }

    public static void setListSecondaryTextStyle(Context mContext, TextView textView) {

        textView.setTypeface(Config.typeFaceRobotoRegular(mContext));
        textView.setTextSize(Config.ITEM_ROW_SECONDARY_TEXT_SIZE);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.lightGray));

    }


    public static void setListTertiaryTextStyle(Context mContext, TextView textView) {

        textView.setTypeface(Config.typeFaceRobotoLight(mContext));
        textView.setTextSize(Config.ITEM_ROW_SECONDARY_TEXT_SIZE);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.darkBlack));

    }


    public static void setTitleTextStyle(Context mContext, TextView textView) {

        textView.setTypeface(Config.typeFaceRobotoRegular(mContext));
        textView.setTextSize(Config.ITEM_TITLE_TEXT_SIZE);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.darkBlack));

    }

    public static void setLightStatusBar(View view, Activity activity) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        }
    }

    public static void customToast(Context context, String string) {

        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();

    }


}
