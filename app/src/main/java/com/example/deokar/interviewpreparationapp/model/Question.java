package com.example.deokar.interviewpreparationapp.model;

public class Question {


    String questionId;
    String questionTitle;
    String questionAnswer;

    public Question(String questionId, String questionTitle, String questionAnswer) {
        this.questionId = questionId;
        this.questionTitle = questionTitle;
        this.questionAnswer = questionAnswer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuestionAnswer() {
        return questionAnswer;
    }

    public void setQuestionAnswer(String questionAnswer) {
        this.questionAnswer = questionAnswer;
    }
}
