package com.example.deokar.interviewpreparationapp;

public class ConnectivityApplication extends AppController {

    private static ConnectivityApplication mInstance;

    public static synchronized ConnectivityApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
