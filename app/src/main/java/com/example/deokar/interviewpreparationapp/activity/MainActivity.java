package com.example.deokar.interviewpreparationapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.deokar.interviewpreparationapp.fragment.OptionListFragment;
import com.example.deokar.interviewpreparationapp.fragment.QuestionListFragment;
import com.example.deokar.interviewpreparationapp.R;

public class MainActivity extends AppCompatActivity {

    private static android.support.v4.app.FragmentManager fragmentManager;

    /**
     * Question List fragment
     */
    public static void questionListFragment() {

        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.frameContainer, new QuestionListFragment(), "tag1").addToBackStack(null)
                .commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frameContainer, new OptionListFragment(), "Tag")
                    .commit();
        }


    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();

        }
    }


}
