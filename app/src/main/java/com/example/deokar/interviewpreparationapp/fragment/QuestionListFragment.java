package com.example.deokar.interviewpreparationapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.deokar.interviewpreparationapp.AppController;
import com.example.deokar.interviewpreparationapp.utils.Config;
import com.example.deokar.interviewpreparationapp.model.Question;
import com.example.deokar.interviewpreparationapp.adapter.QuestionListAdapter;
import com.example.deokar.interviewpreparationapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class QuestionListFragment extends Fragment implements View.OnClickListener {

    String TAG = QuestionListFragment.class.getSimpleName();

    View view;

    Toolbar toolbar;

    RecyclerView questionListView;
    QuestionListAdapter questionListAdapter;
    ArrayList<Question> questionArrayList = new ArrayList<>();

    LinearLayout bannerAdLayout;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_question_list, container, false);


        initView();

        return view;
    }

    /**
     * Initialise views and listeners
     */
    private void initView() {

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.list_page_title);


        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                removeFragment();

            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorPrimary),
                ContextCompat.getColor(getContext(), R.color.colorPrimaryDark)
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (Config.checkConnectivityShowMessage(getContext()))
                    fetchQuestion();
                else
                    mSwipeRefreshLayout.setRefreshing(false);

            }
        });

        questionListView = view.findViewById(R.id.interview_question_list_view);
        bannerAdLayout = view.findViewById(R.id.banner_ad_layout);
        TextView textView = view.findViewById(R.id.ad_text_view);
        textView.setSelected(true);


        bannerAdLayout.setOnClickListener(this);

        if (Config.checkConnectivityShowMessage(getContext()))
            fetchQuestion();
        else
            mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Set recycler view adapter
     */
    private void setAdapter() {

        questionListAdapter = new QuestionListAdapter(getContext(), questionArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        questionListView.setLayoutManager(layoutManager);
        questionListView.setAdapter(questionListAdapter);

    }

    /**
     * Fetch questions api data
     */
    private void fetchQuestion() {

        String url = Config.QUESTION_DATA_API;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.d(TAG, "onResponse: " + jsonObject);
                        parseQuestion(jsonObject);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "Server Error occurred.", Toast.LENGTH_SHORT).show();

            }
        }) {

        };
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    /**
     * Parse data fetched from API
     *
     * @param jsonObject
     */
    private void parseQuestion(JSONObject jsonObject) {

        questionArrayList.clear();

        JSONArray questionArray = jsonObject.optJSONArray("questions");

        for (int i = 1; i <= questionArray.length(); i++) {

            try {
                JSONObject arrayObject = (JSONObject) questionArray.get(i);

                String questionTitle = arrayObject.getString("question");
                String questionAnswer = arrayObject.getString("Answer");


                Question question = new Question(String.valueOf(i), questionTitle, questionAnswer);

                questionArrayList.add(question);


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

//        questionListAdapter.notifyDataSetChanged();

        setAdapter();
        mSwipeRefreshLayout.setRefreshing(false);

    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {

            case R.id.banner_ad_layout:

                Config.openWebsiteInBrowser(getContext(), Config.AD_URL);

                break;

        }


    }

    /**
     * Remove fragment
     */
    private void removeFragment() {

        Fragment fragment = this;
        getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();

    }
}
