package com.example.deokar.interviewpreparationapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.deokar.interviewpreparationapp.R;
import com.example.deokar.interviewpreparationapp.utils.ThemeAndStyle;
import com.example.deokar.interviewpreparationapp.model.Question;

import java.util.ArrayList;

public class QuestionListAdapter extends RecyclerView.Adapter<QuestionListAdapter.MyViewHolder> {

    ArrayList<Question> questionArrayList = new ArrayList<>();
    Context context;
    Question question;


    public QuestionListAdapter(Context context, ArrayList<Question> questionArrayList) {
        this.questionArrayList = questionArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public QuestionListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.question_list_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final QuestionListAdapter.MyViewHolder holder, int position) {

        question = questionArrayList.get(position);

        holder.questionNumberTv.setText(question.getQuestionId());
        holder.questionTitleTv.setText(question.getQuestionTitle());
        holder.questionAnswerTv.setText(question.getQuestionAnswer());

        holder.questionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    holder.answerLayout.setVisibility((holder.answerLayout.getVisibility() == View.VISIBLE)
                            ? View.GONE
                            : View.VISIBLE);


            }
        });

    }

    @Override
    public int getItemCount() {
        return questionArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView questionTitleTv, questionAnswerTv, questionNumberTv;
        LinearLayout answerLayout, questionLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            questionTitleTv = itemView.findViewById(R.id.question_title_tv);
            answerLayout = itemView.findViewById(R.id.answer_layout);
            questionAnswerTv = itemView.findViewById(R.id.answer_text_view);
            questionNumberTv = itemView.findViewById(R.id.question_no_tv);
            questionLayout = itemView.findViewById(R.id.question_layout);

            //Style
            ThemeAndStyle.setListPrimaryTextStyle(context, questionTitleTv);
            ThemeAndStyle.setListPrimaryTextStyle(context, questionAnswerTv);

        }
    }
}
